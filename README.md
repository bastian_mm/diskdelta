# What?

This is a pair of scripts to store and query file and directory sizes in a given directory and subdirectories, including 
time stamps, in a sqlite database. It uses the `du` command for this, which must be available in the path.

It can be used, for example, to find files and directories which grow (or shrink) over time.  

# How?
 
The scripts creates or updates one database file, by default `~/.diskdelta/<DB><DU_OPTS>`, where 

*  `<DB>` is the given or current directory's absolute path, with slashes ("/") replaced with double underscores ("__")
*  `<DU_OPTS>` are the options passed to `du`.  

The database has one table `sizes` with 3 columns:

1.  `name` (TEXT): the full absolute path of the sampled file or directory.
2.  `time` (INTEGER): sample time in Unix time.
3.  `size` (INTEGER): sampled size of the file or directory, in kb with the default `du` options. 
 
# Usage

## Sample

```bash 
diskdelta-sample [-d <directory>] [-o <du options>] [-s <spool directory>]
```

Where 

*  `<directory>` is the directory which is scanned. Default: `.` (current directory).
*  `<du options>` are the options passed to the `du` command. Default: `"-ak"`.
*  `<spool directory>` is the directory where the database file is stored. Default: `~/.diskdelta`.


## Query

One can query the database as usual with `sqlite3 <DB> <sql query>`. See the "How?" section above for a description of 
the schema. 

There is one script to conveniently query the max. difference in size for each file for all stored versions in a given 
time period:

```bash
 diskdelta-query -d <db file> [-p <period seconds>] 
```
Where 

*  `<db file>` is the database file to analyse. This is a required parameter.
*  `<period seconds>` defines the considered time period. All samples between the invocation time of the script and 
`<period seconds>` in the past are considered to find the max. size difference of each file and directory.
 
The results are ordered by the size of the difference, descending.

_Note: there is a `data` command used in this script which uses options available in the MacOS version of `date`. 
To make it work on Linux or other platforms, either adapt the command accordingly or just comment out the line, because
it is only a logging statement._   


# Acknowledgement

These scripts are based on a [script](https://gist.github.com/melnik13/7ad33c57aa33742b9854) originally provided by 
[Vladimir Melnik](https://gist.github.com/melnik13) as a gist.
 
